# setup

```shell
$ git clone https://hmogi@bitbucket.org/hmogi/sample-rails-app.git
$ cd sample-rails-app
$ bundle
$ bundle exec rake db:setup
```

# 確認方法
* 以下のコマンドでサーバーを立ち上げる

```ruby
bundle exec rails s
```

* `http://localhost:3000/users/1.json` にアクセス
    * `{"id":1}` が返る
        * `GroupSerializer` では id のみしか返していない。
        * https://bitbucket.org/hmogi/sample-rails-app/src/f235701cf32f51a943b4f73e4c8dc3a485614227/app/serializers/group_serializer.rb?at=master&fileviewer=file-view-default
    * Group モデルを render メソッドの `json` option に渡しているため、`GroupSerializer` が適応されている

* 以下の差分を適応する

```
--- a/app/controllers/users_controller.rb
+++ b/app/controllers/users_controller.rb
@@ -10,7 +10,7 @@ class UsersController < ApplicationController
   # GET /users/1
   # GET /users/1.json
   def show
-    render json: @user.groups.first
+    render json: @user
   end
```

* 再度 `http://localhost:3000/users/1.json` にアクセス
    * `{"id":1,"name":"user1","age":18}` が返る
        * `id`, `name`, `age` の 3 つを返している
        * https://bitbucket.org/hmogi/sample-rails-app/src/f235701cf32f51a943b4f73e4c8dc3a485614227/app/serializers/user_serializer.rb?at=master&fileviewer=file-view-default
    * User モデルを render メソッドの `json` option に渡しているため、`UserSerializer` が適応されている