class AddForeignKeyUsersToGroups < ActiveRecord::Migration
  def change
    add_foreign_key :groups, :users
  end
end
